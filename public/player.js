export class Player{
   /**
    * Connstructor sets and derives variables from input parameters.
    * @param {*} imgAddr image address; web address or local address to the picture
    * @param {*} keys key bindings to the keyboard; used as references only
    * @param {*} ctx reference to canvas 2D data; used for drawing
    * @param {*} startDirection starting direction of a player
    * @param {*} startLocation starting location of a player
    */
    constructor(imgAddr, keys, ctx, startDirection, startLocation)
    {
        this.ctx = ctx;
        this.image = new Image();
        this.image.src = imgAddr;
        this.keyUp = keys[0];
        this.keyDown = keys[1];
        this.keyLeft = keys[2];
        this.keyRight = keys[3];
        this.speed = 1.0;
        this.direction = startDirection;
        this.position = startLocation;
        this.path = [[...startLocation]];
    }
    
    /**
     * Method managing player update and check of coordinates.
     * @param {*} boundary field bounds for eache edge of the game field
     * @param {*} dt time delta
     * @param {*} speedMultiplier multiplier value used managing speed of a player
     * @returns boolean value whether the player hit boundary or not
     */
    update(boundary, dt, speedMultiplier)
    {
        // wall crash
        if(this.position[0] < boundary[0] || this.position[0] > boundary[1] |
            this.position[1] < boundary[2] || this.position[1] > boundary[3]){
            return false;
        }

        this.position[0] += this.speed * speedMultiplier * dt * this.direction[0];
        this.position[1] += this.speed * speedMultiplier * dt * this.direction[1];
        return true;
    }

    /**
     * Computes colission between the player and lines drawn by both players.
     */
    lineCollision()
    {
        let p1; let p2;
        let d1; let d2;
        if(this.direction[0] == 0){
            p1 = this.position[0] - this.image.width / 2;
            d1 = this.image.width;
            d2 = 2;
            if(this.direction[1] == 1){
                p2 = this.position[1] + this.image.height / 2 + 1;
            }
            else{
                p2 = this.position[1] - this.image.height / 2 - 2;
            }
        }
        else if (this.direction[1] == 0){
            p2 = this.position[1] - this.image.width / 2;
            d1 = 2;
            d2 = this.image.width;
            if(this.direction[0] == 1){
                p1 = this.position[0] + this.image.height / 2 + 1;
            }
            else{
                p1 = this.position[0] - this.image.height / 2 - 2;
            }
        }

        let pixelData = this.ctx.getImageData(p1, p2, d1, d2);
        let pixels = pixelData.data;
        for (let i = 0; i < pixels.length; i+=4){
            if (pixels[i] == 255 && pixels[i + 1] == 0 && pixels[i + 2] == 0){
                return false;
            }
        }
        return true;
    }

    /**
     * Draws polyline following the player's path.
     */
    drawPath()
    {
        let idx = 0;
        let start = this.path[idx];
        let end = [0,0];
        for(idx = 1; idx < this.path.length; idx ++){
            // draw rectangles
            end = this.path[idx];
            this.ctx.fillRect(
                            Math.min(start[0], end[0]) - 1,
                            Math.min(start[1], end[1]) - 1,
                            Math.abs(start[0] - end[0]) + 2,
                            Math.abs(start[1] - end[1]) + 2
            );

            start = this.path[idx];
        }
        //draw final rectangle
        end = this.position;
        this.ctx.fillRect(
            Math.min(start[0], end[0]) - 1,
            Math.min(start[1], end[1]) - 1,
            Math.abs(start[0] - end[0]) + 2,
            Math.abs(start[1] - end[1]) + 2
        );
    }

    /**
     * Method used for drawing the player.
     */
    draw()
    {
        this.ctx.save();
        this.ctx.translate(this.position[0], this.position[1]);                    // move back
        const yRot = this.direction[1] == 1 ? Math.PI : 0;
        this.ctx.rotate(this.direction[0] * Math.PI / 2.0 + yRot);                                                                // rotate
        this.ctx.translate(-this.position[0], -this.position[1]);                   // move to origin
        this.ctx.drawImage(this.image, this.position[0] - this.image.width / 2, this.position[1] - this.image.height / 2);
        this.ctx.restore();
    }

    /**
     * Changes direction of player's path and adds a new node of his path to the path array.
     * @param {*} dir a new direction of the player
     */
    changeDirection(dir)
    {
        if(Math.abs(this.direction[0]) == Math.abs(dir[0]) && Math.abs(this.direction[1]) == Math.abs(dir[1]))
            return;
        this.path.push([...this.position]);
        this.direction = dir;
    }
}
