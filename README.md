# KAJ_TRON

This project is implementation of semestral work in javascript. The theme was the TRON game, where two players try to avoid colliding to each other, to the boundaries and to the paths drawn by each player.

Part of the implementation was the leaderboard, where each player can see his winning rate. The leaderboard is not ordered.

The settings for the first player are WSAD keys, for the second player the arrows. Players do not need to neccessarily move in the grid in the background, the borger, however, counts as a collider.
